package listaTest;


import junit.framework.TestCase;
import model.data_structures.LinkedList;

public class listaTest extends TestCase {

	private LinkedList<Integer> ll;
	int a,b,c,d;
	
	

	private void setupEscenario1() throws Exception
	{
		ll = new LinkedList<Integer>();

		ll.add(12);
		ll.add(5);
		ll.add(16);
		ll.addAtEnd(99);
		ll.addAtK(3, 55);
		
		a = ll.getElement(3);
		b = ll.getElement(ll.getSize());
		c= ll.getElement(1);
		

	}	
	private void setupEscenario2() throws Exception
	{
		ll = new LinkedList<Integer>();

		ll.add(12);
		ll.add(5);
		ll.add(16);
		ll.addAtEnd(99);
		ll.addAtK(3, 55);
				
		a = ll.getElement(1);
		
		ll.delete();
		b = ll.getElement(1);
		
		c= ll.getElement(3);
		ll.deleteAtK(3);
		d = ll.getElement(3);
		
		

	}	
	public void testAdd() throws Exception
	{
		setupEscenario1();
		assertEquals("", "5", ll.getSize().toString());
		assertEquals("", 55, a);
		assertEquals("", 99, b);
		assertEquals("", 16, c);

	}
	
	public void testDelete() throws Exception
	{
		setupEscenario2();
		assertEquals("", 16, a);
		assertEquals("", 5, b);
		assertEquals("", 12, c);
		assertEquals("", 99, d);
		
	}

}