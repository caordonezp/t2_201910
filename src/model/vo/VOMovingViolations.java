package model.vo;

import model.data_structures.LinkedList;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {

	private int objectId, totalPaid;
	private String location,ticketIssueDate,accidentIndicator,violationDescription;
	private LinkedList<VOMovingViolations> lista;
	
	public VOMovingViolations()
		
	}
	
	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		
		return objectId;
	}	
	
	
	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		
		return ticketIssueDate;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		
		return totalPaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		
		return accidentIndicator;
	}
		
	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {

		return violationDescription;
	}
}
