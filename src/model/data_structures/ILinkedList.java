package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable<T> {
	
	
	public void add(T item);
	
	public void addAtEnd(T item);
	
	public void addAtK(int k, T item) throws Exception;
	
	public T getElement(int pos) throws Exception;
	
	public Integer getSize();
	
	public void delete() throws Exception;
	
	public void deleteAtK(int k) throws Exception;
	


}
