package model.data_structures;

public class Node<E> 
{
	//Atributos

	private Node<E> sig, ant;

	private E item;

	//Constructor

	public Node(E item) {
		sig = null;
		ant = null;
		this.item = item;
	}
/**
 * asigna el siguiente elemento al actual.
 * @param sig es el elemento que se pondr� despu�s del actual en la lista
 */
	public void setSig(Node<E> sig) {
		this.sig = sig;
	}
	/**
	 * asigna el anterior elemento al actual
	 * @param ant es el elemento que se pondr� antes del actual en la lista
	 */
	public void setAnt(Node<E> ant) {
		this.ant = ant;
	}
	
	/**
	 * da el siguiente elemento al actual
	 * @return el elemento en la posici�n de adelante
	 */
	public Node<E> getSig() {
		return sig;
	}
	/**
	 * da el elemento anterior al actual
	 * @return elemento en la posici�n anterior
	 */
	public Node<E> getAnt(){
		return ant;
	}
/**
 * da el item que est� almacenado en el nodo
 * @return item almacenado en el nodo
 */
	public E getDato() {
		return item;
	}
/**
 * le a�ade a un nodo un item
 * @param item el elemento a a�adir a un nodo
 */
	public void setItem(E item)
	{
		this.item = item;
	}
}