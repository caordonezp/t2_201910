package model.data_structures;

import java.util.Iterator;

public class LinkedList <T> implements ILinkedList<T>{

	//Atributos
	private Node<T> cabeza, cola;
	private T item;
	private int size;
	//Constructor
	public LinkedList()
	{
		item = null;
		cabeza = new Node<T> (item);
		cola = new Node<T> (item);
		size = 0;

	}

	
	public Iterator<T> iterator() {

		return new Iterator<T>(){
			Node<T> act = cabeza;
			public boolean hasNext(){
				if(size == 0)
				{
					return false;
				}
				if(act == null)
				{
					return true;
				}
				return act.getSig() != null;

			}

			public T next() {
				if(act == null)
					act = cabeza;
				else
					act = act.getSig();

				return act.getDato();
			}
		};
	}

//M�todos
	/**
	 * a�ade un item en la primera posici�n de la lista. 
	 * Si la lista est� vac�a, lo asigna como cabeza y cola.
	 * Si no est� vac�a, cambia la cabeza y le asigna como siguiente la anterior cabeza
	 */
	public void add(T item) {
		Node<T> nuevoNodo=new Node<T>(item);
		if(cabeza == null)
		{
			cabeza = nuevoNodo;
			cola = nuevoNodo;
			nuevoNodo.setSig(null);
			nuevoNodo.setAnt(null);
		}
		else{
			nuevoNodo.setSig(cabeza);
			cabeza.setAnt(nuevoNodo);
			cabeza=null;
			cabeza=nuevoNodo;
		}

		size++;

	}

/**
 * a�ade un elemento al final de la lista. Si la lista est� vac�a, lo a�ade de primeras.
 */
	public void addAtEnd(T item) {
		Node<T> nuevoNodo=new Node<T>(item);
		if(cola==null)
		{
			cabeza=nuevoNodo;
			cola=nuevoNodo;
			nuevoNodo.setSig(null);
			nuevoNodo.setAnt(null);
		}
		else
		{
			nuevoNodo.setAnt(cola);
			cola.setSig(nuevoNodo);
			cola=nuevoNodo;
		}

		size++;
	}

/**
 * a�ade un elemento en la posici�n dada. Si es la primera posici�n, a�ade en la cabeza.
 * Si es la �ltima posici�n, a�ade en la cola. 
 * @throws lanza excepci�n si se ingresa una posici�n que no existe en la lista
 */
	public void addAtK(int k, T item) throws Exception {

		if(k-1 == 0)
		{
			add(item);
		}

		else if (k == size)
		{
			addAtEnd(item);
		}

		else if (k-1 >= size)
			throw new Exception("la posici�n excede el tama�o de la lista");

		else
		{
			Node<T> nuevoNodo = new Node<T>(item);
			Node<T> actual = cabeza;
			Node<T> anterior = null;
			for(int i = 0; i<k-1; i++)
			{
				actual = actual.getSig();
				anterior = actual.getAnt();
			}

			nuevoNodo.setAnt(anterior);
			nuevoNodo.setSig(actual);
			actual.setAnt(nuevoNodo);
			anterior.setSig(nuevoNodo);

			size++;
		}
	}

	/**
	 * regresa el elemento en la posici�n dada
	 * @throws lanza excepci�n si en la posici�n no hay nada o no existe la posici�n
	 */

	public T getElement(int pos) throws Exception
	{
		Node<T> actual = cabeza;

		if(pos - 1 == 0)
		{
			actual = cabeza;
		}

		else if (pos == size)
		{
			actual = cola;
		}

		else if (pos-1 >= size)
			throw new Exception("La posici�n dada no se encuentra dentro de la lista");

		else
		{
			int cont=0;
			for(int i = 0; i < size; i++)
			{
				cont++;
				actual = actual.getSig();
				if (cont == pos-1)
				{
					return actual.getDato();
				}

			}


		}

		return actual.getDato();
	}


	public Integer getSize() 
	{

		return size;
	}

	/**
	 * elimina el primer elemento de la lista.
	 * @throws lanza excepci�n si la lista est� vac�a
	 */

	public void delete() throws Exception 
	{

		if(cabeza == null)
		{
			throw new Exception ("La lista est� vac�a, no hay nada para borrar");
		}
		else 
		{
			Node<T> borrar = cabeza;
			Node<T> sig = borrar.getSig();

			sig.setAnt(null);
			cabeza = null;
			cabeza = sig;

		}
		size--;


	}

/**
 * elimina el elemento en la posici�n dada.
 * @throws lanza excepci�n si la posici�n no existe en la lista o si est� vac�a
 */
	
	public void deleteAtK(int k) throws Exception
	{
		if(k-1 == 0)
		{
			delete();
		}

		else if (k == size)
		{
			Node<T> borrar = cola;
			Node<T> ant = borrar.getAnt();
			ant.setSig(null);
			cola = null;
			cola = ant;
			size--;


		}

		else if (k-1 >= size )
			throw new Exception("No se pudo borrar: la posici�n no est� en la lista");

		else
		{
			Node<T> borrar = cabeza;
			Node<T> sig = borrar.getSig();
			Node<T> ant = null;
			for(int i = 0; i<k-1; i++)
			{
				borrar = borrar.getSig();
				ant = borrar.getAnt();
				sig = borrar.getSig();
			}

			ant.setSig(sig);
			sig.setAnt(ant);

			size--;
		}


	}

}

